package VehicleDemo;

public class Car extends Vehicle {
	
	private String fueltype;
	
	Car(String make, String model, int year, String fueltype)
	{
		super(make, model, year);
		this.fueltype = fueltype;
	}
	
	void print () 
	{
		super.print();
		System.out.println("FuelType : " + fueltype);
	}
}
